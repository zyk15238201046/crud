package com.kang.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Information {
    @TableId(type = IdType.AUTO)
    private Long Id;
    private String title;
    private String introduce;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @DateTimeFormat(fallbackPatterns = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    @DateTimeFormat(fallbackPatterns = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate;
    private int delFlag;
}
