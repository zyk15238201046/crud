package com.kang.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kang.domain.Information;
import org.springframework.stereotype.Service;

public interface InformationService extends IService<Information> {
}
