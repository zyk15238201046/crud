package com.kang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.kang.**"})
public class CrudMain {
    public static void main(String[] args) {
        SpringApplication.run(CrudMain.class,args);
    }
}
