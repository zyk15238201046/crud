package com.kang.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kang.domain.Information;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InformationMapper extends BaseMapper<Information> {
}
