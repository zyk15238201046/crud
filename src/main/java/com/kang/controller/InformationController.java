package com.kang.controller;

import com.kang.domain.Information;
import com.kang.service.InformationService;
import com.kang.service.impl.InformationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/test")
public class InformationController {

    @Autowired
    private InformationServiceImpl informationService;

    @RequestMapping("/list")
    public List<Information> queryList(){
        List<Information> list = informationService.list();
        return list;
    }
    @RequestMapping("/create")
    public String create(@RequestBody Information information){
        information.setCreateDate(new Date());
        return informationService.save(information)?"新增成功":"新增失败";
    }
    @RequestMapping("/update")
    public String update(@RequestBody Information information){
        information.setUpdateDate(new Date());
        return informationService.updateById(information)?"修改成功":"修改失败";
    }
    @RequestMapping("delete/{id}")
    public String delete(@PathVariable int id){
        return informationService.removeById(id)?"删除成功":"删除失败";
    }
}
